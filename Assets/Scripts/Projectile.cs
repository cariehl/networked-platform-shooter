﻿using UnityEngine;
using System.Collections;

// Attach to a prefab
// Needs rigidbody2D

public enum ProjectileType {
	PROJ_PISTOL,
	PROJ_MACHINE_GUN,
	PROJ_SHOTGUN,
	PROJ_SNASER
}

public enum ProjectileTarget {
	ANY_PLAYER,
	ANY_MOB,
}

public static class ProjectileDB{


	// Damage amount for each projectile type
	public static int[] Damage = new int[]{
		4, 
		1,
		2, // per bullet
		10 
	};

	// Speed for each proj. type. (Force amount)
	public static int[] Speed = new int[]{
		2,
		8,
		8,
		8,
	};

	// The Proj. lifetime in seconds
	public static float[] Lifetime = new float[]{
		1.0f,
		1.2f,
		.5f,
		5

	};
}





public class Projectile : Photon.MonoBehaviour {
	private ProjectileType type;
	private float direction;
	private int originID;
	public ProjectileTarget targetClass;

	float liveTime;

	private bool valid;

    [RPC]
	public void initialize(ProjectileType p, float d, int CreatorID, ProjectileTarget t) {
		type = p;
		direction = d;
		originID = CreatorID;
		targetClass = t;
		valid = true;

		//PhotonNetwork.Destroy (this.gameObject, ProjectileDB.Lifetime[(int)type]);
	}

	// Use this for initialization
	void Start () {
		liveTime = ProjectileDB.Lifetime[(int)type];

		float newXcomp = ProjectileDB.Speed [(int)type] * (Mathf.Cos (Mathf.Deg2Rad * direction));
		float newYcomp = ProjectileDB.Speed [(int)type] * (Mathf.Sin (Mathf.Deg2Rad * direction));
       
		rigidbody2D.AddForce (new Vector2 (newXcomp, newYcomp));
        Debug.Log("force vector " + rigidbody2D.velocity);
	}
	
	// Update is called once per frame
	void Update () {
		liveTime -= Time.deltaTime;
		if (liveTime <= 0 && valid) {
			valid = false;
			PhotonNetwork.Destroy(this.gameObject);
		}
	}

	public int getDamage() {
		if (!gameObject) return -1;
		return ProjectileDB.Damage[(int)type];
	}

	public bool isValidCollision(GameObject other) {
		if ((gameObject && other.GetInstanceID () != originID && valid || other.tag == tag)) {
			valid = false;
			PhotonNetwork.Destroy (this.gameObject);
            return true;
		}
        Physics2D.IgnoreCollision(other.collider2D, this.gameObject.collider2D);
		return false;
	}

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }

    void OnCollisionEnter2D(Collision2D col)
    {
		Physics2D.IgnoreCollision(this.gameObject.collider2D, col.collider);
    }
}
