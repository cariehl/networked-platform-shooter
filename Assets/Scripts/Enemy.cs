﻿using UnityEngine;
using System.Collections;



public class Enemy : MonoBehaviour {

	const int base_health_c = 100;
	const int base_attack_c = 10;
	const float base_speed_c = 4;
	const float base_jump_height_c = 14;
	const int projectile_refire_time_c = 30;
	const float base_dist = 4.0f;
	const int jump_grace_period = 300; // steps;

    float randomgun;
    float randomdrop;

	private int 	health;
	private int 	attack;
	private bool 	inTheAir;
	private int 	projectileCounter;
	private float 	closestDist;
	private int 	jumpCounter;

	// Set the prefab that houses the projectile in the Unity script component interface
	public Projectile p;
	
	/* Base Logic */
	void Start () {
		health = base_health_c;
		attack = base_attack_c;
		inTheAir = false;
		projectileCounter = 0;
		closestDist = base_dist;
		jumpCounter = 0;
	}
	
	// Update is called once per frame
	void Update () {

		projectileCounter--;
		jumpCounter--;

		Vector2 target = new Vector2(4, 4);

		if (projectileCounter < 0) {
			shoot(target);
			
		}

	

		if (transform.position.x < target.x) {
			moveRight ();
		}

		if (transform.position.x > target.x) {
			moveLeft ();
		}

		if (transform.position.y > target.y) {
			jump ();
		}

		if (health <= 0) {
			die();
		}
	}





	/* Movement Controls */
	public void moveLeft() {
		rigidbody2D.AddForce(new Vector2 (-base_speed_c, 0));
	}

	public void moveRight() {
		rigidbody2D.AddForce (new Vector2 (base_speed_c, 0));
	}

	public void jump() {
		if (!inTheAir && jumpCounter < 0) { 
			rigidbody2D.AddForce (new Vector2(1, 0));
			inTheAir = true;
			jumpCounter = jump_grace_period;
		}
	}




	/* Utility FUnctions */
	private void takeDamage(Projectile p) {
		health -= p.getDamage();
		// create particles or something
		Destroy(p);
	}

	private void die() {
		// create more particles 
		Destroy (this.gameObject);

        randomgun = Random.Range(1.0f, 100f);
        randomdrop = Random.Range(1.0f, 3.0f);
        if(randomgun <= 7.0f){
            //instantiate machine gun
        }
        else if ((randomgun > 7.0f) && (randomgun <= 14.0f)) { 
            //instantiate shotgun
        }
        else if ((randomgun > 14.0f) && (randomgun <= 21.0f)) { 
            //instantiate sniper
        }

        if (randomdrop <= 2.0f) { 
            //drop bonus health
        }
	}

	// Returns the direction of the given point
	private float getDirection(Vector2 v) {
		return Mathf.Atan ( (transform.position.y - v.y) / (transform.position.x - v.x)) * Mathf.Rad2Deg;
	}

	//
	private void shoot(Vector2 target) {
		makeBullet(getDirection(target), 5);
		projectileCounter = projectile_refire_time_c;
	}



	// Produces a bullet.
	// direction is the degree value for the bullets direction
	// variance is the degrees of spread.
	private void makeBullet(float direction, float variance) {

		Projectile pNew = (Projectile) Instantiate (p, transform.position, transform.rotation);
		pNew.initialize(ProjectileType.PROJ_PISTOL,
		          direction + (variance*Random.Range (-1.0f, 1.0f)) , 
		          gameObject.GetInstanceID(), 
		          ProjectileTarget.ANY_PLAYER);

	}


	// Example Bullet Collider.
	// Requires Projectiles to have the tag "Projectile"
	public void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject && col.gameObject.tag == "Projectile") {
			Projectile pCol = col.gameObject.GetComponent<Projectile>();

			// First check to see if the collieded Bullet was spawned by us. If so, ignore it.
			// ALways run this check!
			if (pCol.isValidCollision(this.gameObject)) {
				takeDamage(pCol);
			}
		}
	}
}	
