﻿using UnityEngine;
using System.Collections;

public class NetworkEnemy : Photon.MonoBehaviour {

    Vector3 realPosition = Vector3.zero;
    Quaternion armRotation = Quaternion.identity;
    Quaternion bodyRotation = Quaternion.identity;
    Vector3 spriteScale = Vector3.zero;

    GameObject arm;
    GameObject body;
    GameObject sprite;
	GameObject hand;

    Animator anim;

	// Use this for initialization
	void Start () {
        sprite = transform.FindChild("sprite").gameObject;
        body = transform.FindChild("sprite/base").gameObject;
        arm = transform.FindChild("sprite/base/arm").gameObject;
		hand = transform.FindChild("sprite/base/arm/hand").gameObject;

        anim = GetComponent<Animator>();
        if (anim == null) {
            Debug.LogError("Forgot to put animator on enemy prefab");
        }
	}
	
	// Update is called once per frame
	void Update () {
		if(!PhotonNetwork.isMasterClient) {
            transform.position = Vector3.Lerp(transform.position, realPosition, 0.3f);
            body.transform.rotation = Quaternion.Lerp(body.transform.rotation, bodyRotation, 0.35f);
            arm.transform.rotation = Quaternion.Lerp(arm.transform.rotation, armRotation, 0.35f);

            sprite.transform.localScale = spriteScale;
        }
	}

	public void Kill()
	{

		GameObject.Find("_Script Objects").GetComponent<PhotonView>().RPC("EnemyDeath", PhotonTargets.All);
		PhotonNetwork.Destroy(this.gameObject);
	}

    void OnPhotonInstantiate()
    {
		if (!PhotonNetwork.isMasterClient) {
            Destroy(rigidbody2D);
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
		if (stream.isWriting) {
            // We are the host and we need to send enemy information to the other players
			Debug.Log("sendEnemyInfo");

            stream.SendNext(transform.position);
            stream.SendNext(sprite.transform.localScale);
            stream.SendNext(arm.transform.rotation);
            stream.SendNext(body.transform.rotation);

            stream.SendNext(anim.GetFloat("Speed"));
            stream.SendNext(anim.GetFloat("VertSpeed"));
            stream.SendNext(anim.GetBool("Ground"));
        } else { 
            // We are receiving enemy information from the host
            
            realPosition = (Vector3)stream.ReceiveNext();
            spriteScale = (Vector3)stream.ReceiveNext();
            armRotation = (Quaternion)stream.ReceiveNext();
            bodyRotation = (Quaternion)stream.ReceiveNext();

            anim.SetFloat("Speed", (float)stream.ReceiveNext());
            anim.SetFloat("VertSpeed", (float)stream.ReceiveNext());
            anim.SetBool("Ground", (bool)stream.ReceiveNext());
        }
    }
}
