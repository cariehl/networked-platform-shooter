﻿using UnityEngine;
using System.Collections;

public class PlayerController : Photon.MonoBehaviour
{
    public float maxSpeed = 10f;
    bool facingright = true;

    Animator anim;

    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.1f;
    public LayerMask whatIsGround;
    public float jumpForce = 180f;

    bool doubleJump = false;
	bool pvpenabled;

	GameObject arm;
	GameObject hand;
	GameObject body;
	GameObject sprite;
	public GameObject gun;
	public Camera playerCam;

    bool mg = false;
    bool shotgun = false;
    bool sniper = false;

    int mgAmmo = 0;
    int shotgunAmmo = 0;
    int sniperAmmo = 0;

    public int gunActive = 0; // 0 = pistol, 1 = MG, 2 = shotgun, 3 = sniper

    float mouseDir;
    int gunCounter = 0;
    const int gunMax = 20;

    GameObject bulletPrefab;
    public float gun1FireRate = 1.0f;
    float cooldown = 0;


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Crate") {
            fullRefillAmmo(gunActive);
        }
        else {
            switch(other.gameObject.tag) {
                case "Player1": 
                case "Player2":
                case "Player3":
                case "Player4":
                    Destroy (other.gameObject);
                    break;
            }
            gunPickUp(other);
        }
    }

    void gunPickUp(Collider2D other)
    {
        if (other.gameObject.tag == "gunMG") {
            if (!mg) {
                mg = true;
                mgAmmo = 100;
            }
            else {
                mgAmmo += 20;
                if (mgAmmo > 100) {
                    mgAmmo = 100;
                }
            }
        }
        else if (other.gameObject.tag == "gunShotgun"){
            if (!shotgun) {
                shotgun = true;
                shotgunAmmo = 20;
            }
            else {
                shotgunAmmo += 4;
                if (shotgunAmmo > 20) {
                    shotgunAmmo = 20;
                }
            }
        }
        else if (other.gameObject.tag == "gunSniper"){
            if (!sniper) {
                sniper = true;
                sniperAmmo = 10;
            }
            else {
                sniperAmmo += 2;
                if (sniperAmmo > 10) {
                    sniperAmmo = 10;
                }
            }
        }
    }

    void fullRefillAmmo(int activeGun) {
        if (activeGun == 1) {
            mgAmmo = 100;
        }
        else if (activeGun == 2) {
            shotgunAmmo = 20;
        }
        else if (activeGun == 3) {
            sniperAmmo = 10;
        }
    }
    
	void ArmLookAtMouse()
	{
		var mousePos = Input.mousePosition;
		mousePos.z = (0 - playerCam.transform.position.z);
		Vector3 worldp = playerCam.ScreenToWorldPoint(mousePos);
		Vector3 diff = worldp - hand.transform.position;
		diff.Normalize();
		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        
        mouseDir = rot_z;
       

		if (!facingright) {
			if (rot_z > 0)
				rot_z = 180 - rot_z;
			else
				rot_z = -180 - rot_z;
		}
		
		// If the mouse goes past our character's center (left/right), turn the other way
        
  
		if (rot_z < -90 || rot_z > 100) {
			Flip ();
            //mouseDir = 180 - mouseDir;
		}
        
        

        // Set rotation of character
		arm.transform.localRotation = Quaternion.Euler(0f, 0f, rot_z * 0.8f);
		body.transform.localRotation = Quaternion.Euler(0f, 0f, rot_z * 0.2f);
	}

	void Start()
	{
		sprite = transform.FindChild("sprite").gameObject;
		body = transform.FindChild("sprite/base").gameObject;
		arm = transform.FindChild("sprite/base/arm").gameObject;
		hand = transform.FindChild("sprite/base/arm/hand").gameObject;

		GameObject my_gun = Instantiate(gun) as GameObject;
		my_gun.transform.parent = hand.transform;
		my_gun.transform.localPosition = Vector3.zero;
		my_gun.renderer.sortingOrder = my_gun.transform.parent.parent.renderer.sortingOrder;

		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool("Ground", grounded);
		
		/*        if (grounded) {
            doubleJump = false;
        }*/
		
		float move = Input.GetAxis ("Horizontal");
		anim.SetFloat("Speed", Mathf.Abs(move));
		anim.SetFloat("VertSpeed", rigidbody2D.velocity.y);
		
		rigidbody2D.velocity = new Vector2(move * maxSpeed, rigidbody2D.velocity.y);

		ArmLookAtMouse();
	}

    void Update() 
    {
        gunCounter++;
        if ((Input.GetKeyDown(KeyCode.Space))) {
            if (grounded) {
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
                rigidbody2D.AddForce(new Vector2(0, jumpForce));
                doubleJump = true;            
            }
            else {
                if (doubleJump) {
					rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
                    rigidbody2D.AddForce(new Vector2(0, jumpForce));
                    doubleJump = false;
                }
            }
        }

        //cooldown -= Time.deltaTime;
        //if (cooldown <= 0) {
        if (gunCounter >= gunMax) {
            cooldown = gun1FireRate;
            if (Input.GetButton("Fire1")) {
                if (photonView.isMine) {
                    instaBullet(mouseDir, 0);
					//photonView.RPC ("Fire", PhotonTargets.All, new PhotonMessageInfo());
                }
            }
            gunCounter = 0;
        }
    }

    void Flip () 
    {
        facingright = !facingright;
        Vector2 theScale = sprite.transform.localScale;
        theScale.x *= -1;

        // Flip the character
        sprite.transform.localScale = theScale;
    }

    void Fire()
    {
        if (cooldown > 0) {
            return;
        }
        cooldown = gun1FireRate;
    }

    [RPC]
    void Fire(PhotonMessageInfo info)
    {
        if (info.sender.ID != PhotonNetwork.player.ID)
        {
            instaBullet(mouseDir, 0);
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

    }

    void instaBullet(float direction, float variance)
    {
        GameObject pObj = PhotonNetwork.Instantiate("Proj", hand.transform.position, transform.rotation, 0);
        Projectile pNew = pObj.GetComponent<Projectile>();

        //pNew.photonView.RPC("initialize", PhotonTargets.All, ProjectileType.PROJ_SNASER, 0.1f, 4, 5);
        	
		    pNew.photonView.RPC("initialize", PhotonTargets.All, 

                      ProjectileType.PROJ_PISTOL, 
		              (float) (direction + (variance*Random.Range (-1.0f, 1.0f))) , 
		              gameObject.GetInstanceID(), 
		              NetworkManager.pvpenabled ? ProjectileTarget.ANY_PLAYER : ProjectileTarget.ANY_MOB);  

		audio.Play ();
			
		//pNew.photonView.RPC ("initialize", 0, 0.0f, 3, 5);
    }

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Projectile" && col.gameObject.GetComponent<Projectile>().targetClass == ProjectileTarget.ANY_PLAYER &&
		    col.gameObject.GetComponent<Projectile>().isValidCollision(gameObject)) {
			GetComponent<Health>().TakeDamage(col.gameObject.GetComponent<Projectile>().getDamage());
			PhotonNetwork.Destroy(col.gameObject);
		} else if (col.gameObject.tag == "Projectile") {
			Physics2D.IgnoreCollision(this.collider2D, col.collider);
		}
	}
}
