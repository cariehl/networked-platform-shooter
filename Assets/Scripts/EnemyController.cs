﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
    public float maxSpeed = 10f;
    bool facingright = true;

    Animator anim;

    bool grounded = false;
    public Transform groundCheck;
    float groundRadius = 0.1f;
    public LayerMask whatIsGround;
    public float jumpForce = 180f;

	int gunCounter = 0;
	const int gunMax = 60;
	public float gun1FireRate = 1.0f;
	float cooldown = 0;

    bool doubleJump = false;

	GameObject target;

	GameObject arm;
	GameObject hand;
	GameObject body;
	GameObject sprite;
	public GameObject gun;

    int gunActive = 0; // 0 = pistol, 1 = MG, 2 = shotgun, 3 = sniper

	void AimAt(Vector3 targetWorldPos)
	{
		if (target == null) {
			arm.transform.localRotation = Quaternion.Euler(0f, 0f, 90.0f * 0.8f);
		}
		Vector3 diff = targetWorldPos - hand.transform.position;
		diff.Normalize();
		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

		if (!facingright) {
			if (rot_z > 0)
				rot_z = 180 - rot_z;
			else
				rot_z = -180 - rot_z;
		}
		
		// If the mouse goes past our character's center (left/right), turn the other way
		if (rot_z < -90 || rot_z > 100) {
			Flip ();
		}
		
		arm.transform.localRotation = Quaternion.Euler(0f, 0f, rot_z * 0.8f);
		if (facingright)
			body.transform.localRotation = Quaternion.Euler(0f, 0f, rot_z * 0.2f);
		else
			body.transform.localRotation = Quaternion.Euler(0f, 0f, rot_z * 0.2f);
	}

	float MoveTo(Vector3 targetWorldPos)
	{
		if (transform.position.x > targetWorldPos.x + 2.0f)
			return -0.5f;
		else if (transform.position.x < targetWorldPos.x - 2.0f)
			return 0.5f;
		return 0f;
	}

	void FindClosestPlayer()
	{
		ArrayList potentialTargets = new ArrayList();
		
		potentialTargets.Add(GameObject.FindGameObjectWithTag("Player1"));
		potentialTargets.Add(GameObject.FindGameObjectWithTag("Player2"));
		potentialTargets.Add(GameObject.FindGameObjectWithTag("Player3"));
		potentialTargets.Add(GameObject.FindGameObjectWithTag("Player4"));
		
		float lowestDist = Mathf.Infinity;
		foreach(GameObject _target in potentialTargets) {
			if (_target != null) {
				float distance = (_target.transform.position - transform.position).sqrMagnitude;
				if (distance < lowestDist) {
					lowestDist = distance;
					target = _target;
				}
			}
		}
	}

	void Start()
	{
		sprite = transform.FindChild("sprite").gameObject;
		body = transform.FindChild("sprite/base").gameObject;
		arm = transform.FindChild("sprite/base/arm").gameObject;
		hand = transform.FindChild("sprite/base/arm/hand").gameObject;

		GameObject my_gun = Instantiate(gun) as GameObject;
		my_gun.transform.parent = hand.transform;
		my_gun.transform.localPosition = Vector3.zero;
		my_gun.renderer.sortingOrder = my_gun.transform.parent.parent.renderer.sortingOrder;

		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
		anim.SetBool("Ground", grounded);

		FindClosestPlayer();
		if (target) {
			AimAt (target.transform.position);
			float move = MoveTo (target.transform.position);
			anim.SetFloat("Speed", Mathf.Abs(move));
			anim.SetFloat("VertSpeed", rigidbody2D.velocity.y);
			
			rigidbody2D.velocity = new Vector2(move * maxSpeed, rigidbody2D.velocity.y);
		}


	}

    void Update() 
    {
		gunCounter++;
		if (gunCounter >= gunMax) {
			cooldown = gun1FireRate;
			if (PhotonNetwork.isMasterClient) {
				Debug.Log ("Fire!");
				instaBullet(0);
			}
			gunCounter = 0;
		}
    }

	void instaBullet(float variance)
	{
		if (!target) return;
		Vector3 diff = target.transform.position - hand.transform.position;
		diff.Normalize();
		float rotation = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;

		GameObject pObj = PhotonNetwork.Instantiate("Proj", hand.transform.position, transform.rotation, 0);
		Projectile pNew = pObj.GetComponent<Projectile>();
		
		//pNew.photonView.RPC("testFunc", PhotonTargets.All, ProjectileType.PROJ_SNASER, 0.1f, 4);
		
		pNew.photonView.RPC("initialize", PhotonTargets.All, 
		                    new object[]{ProjectileType.PROJ_PISTOL, 
			rotation + (variance*Random.Range (-1.0f, 1.0f)) , 
			gameObject.GetInstanceID(), 
			ProjectileTarget.ANY_PLAYER});

		audio.Play ();
	}

    void Flip () 
    {
        facingright = !facingright;
        Vector2 theScale = sprite.transform.localScale;
        theScale.x *= -1;
        sprite.transform.localScale = theScale;
    }

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Projectile" && col.gameObject.GetComponent<Projectile>().targetClass == ProjectileTarget.ANY_MOB &&
		    col.gameObject.GetComponent<Projectile>().isValidCollision(gameObject)) {
			GetComponent<Health>().TakeDamage(col.gameObject.GetComponent<Projectile>().getDamage());
			if (col.gameObject.GetInstanceID() == target.GetInstanceID()) {
				target = null;
			}
			PhotonNetwork.Destroy(col.gameObject);

		} else if (col.gameObject.tag == "Projectile") {
			Physics2D.IgnoreCollision(this.collider2D, col.collider);
		}
	}
}
