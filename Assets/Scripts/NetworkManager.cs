﻿using UnityEngine;
using System.Collections.Generic;



public class NetworkManager : Photon.MonoBehaviour {

    public bool offlineMode = false;
    float roundstartwait = 2.0f;
	float spawnTimer = 0.0f;
	public float SPAWN_TIME = 15.0f;
	int playerCount = 0;

	bool boolclick = false;

	public Texture thetexture;
	public int MAX_SPAWN = 1;

	bool keepSpawning = true;
	public AudioClip menuSong;
	public AudioClip pveSong;

	GameObject myPlayer;

    public GameObject standbyCamera;

    bool connecting = false;

	public static bool pvpenabled;

    List<string> chatMessages = new List<string>();
    int maxChatMessages = 5;
    float msgDisplayTime = 3.0f;
    float msgTimeRemaining = 0.0f;

    bool lobbystarted = false;
    int Player1ready = 0;
    int Player2ready = 0;
    int Player3ready = 0;
    int Player4ready = 0;
	int numEnemiesSpawned = 0;
	int numEnemiesKilled = 0;
    bool roundstart = false;
    int totalPlayers = 0;
    int readycount = 0;
    bool first = false;

    void Start()
    {
		audio.clip = menuSong;
		audio.Play ();
        PhotonNetwork.player.name = PlayerPrefs.GetString("Username", "Space Cowboy");
    }

    void OnDestroy()
    {
        PlayerPrefs.SetString("Username", PhotonNetwork.player.name);

    }

	void Update()
	{
        totalPlayers = PhotonNetwork.playerList.Length;
		if (PhotonNetwork.isMasterClient) {
			if (roundstartwait <= 0f && keepSpawning) {
				if (spawnTimer <= 0f) {
					spawnTimer = SPAWN_TIME;
					spawnMobs();
				} else {
					spawnTimer -= Time.fixedDeltaTime;
				}
			} else if(roundstart){
				roundstartwait -= Time.fixedDeltaTime;
			}
		}
        msgTimeRemaining -= Time.deltaTime;
	}

    void Connect()
    {
        PhotonNetwork.ConnectUsingSettings("1.0.0");
        
    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());



        if (PhotonNetwork.connected == false && connecting == false) {
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Username: ");
            PhotonNetwork.player.name = GUILayout.TextField(PhotonNetwork.player.name);
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Single Player")) {
                connecting = true;
                PhotonNetwork.offlineMode = true;
                OnJoinedLobby();
            }

            if (GUILayout.Button("Multiplayer")) {
                connecting = true;
                lobbystarted = true;
                Connect();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        if (PhotonNetwork.connected == true && connecting == false) {
            if (msgTimeRemaining > 0) {
                Color old_color = GUI.color;
                float alpha = 1f;
                if (msgTimeRemaining < 1f)
                {
                    alpha = msgTimeRemaining;
                }
                Color c = new Color(old_color.r, old_color.g, old_color.b, alpha);
                GUI.color = c;
                GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
                GUILayout.BeginVertical();
                GUILayout.FlexibleSpace();

                foreach (string msg in chatMessages)
                {
                    GUILayout.Label(msg);
                }
                GUILayout.EndVertical();
                GUILayout.EndArea();
                GUI.color = old_color;
            }
        }

        if (roundstart == false && lobbystarted == true) {
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Connected Players");
            GUILayout.EndHorizontal();
            for (int j = 0; j < totalPlayers; ++j) {
                if(j == 0){
                    GUILayout.Label(PhotonNetwork.playerList[j].name);
                    GUILayout.BeginHorizontal();
                    if(GUILayout.Button("Ready: ")){
                        if(Player1ready == 1){
                            photonView.RPC("Player1Var", PhotonTargets.AllBuffered, 0);
                        }
                        else{
                            photonView.RPC("Player1Var", PhotonTargets.AllBuffered, 1);
                        }
                    }
                    if(Player1ready == 1)
                        GUILayout.Label("yes");
                    else
                        GUILayout.Label("no");
                    GUILayout.EndHorizontal();
                }
                else if(j == 1){
                    GUILayout.Label(PhotonNetwork.playerList[j].name);
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Ready: "))
                    {
                        if (Player2ready == 1)
                        {
                            photonView.RPC("Player2Var", PhotonTargets.AllBuffered, 0);
                        }
                        else
                        {
                            photonView.RPC("Player2Var", PhotonTargets.AllBuffered, 1);
                        }
                    }
                    if (Player2ready == 1)
                        GUILayout.Label("yes");
                    else
                        GUILayout.Label("no");
                    GUILayout.EndHorizontal();
                }
                else if(j == 2){
                    GUILayout.Label(PhotonNetwork.playerList[j].name);
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Ready: "))
                    {
                        if (Player3ready == 1)
                        {
                            photonView.RPC("Player3Var", PhotonTargets.AllBuffered, 0);
                        }
                        else
                        {
                            photonView.RPC("Player3Var", PhotonTargets.AllBuffered, 1);
                        }
                    }
                    if (Player3ready == 1)
                        GUILayout.Label("yes");
                    else
                        GUILayout.Label("no");
                    GUILayout.EndHorizontal();
                }
                else if(j == 3){
                    GUILayout.Label(PhotonNetwork.playerList[j].name);
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Ready: "))
                    {
                        if (Player4ready == 1)
                        {
                            photonView.RPC("Player4Var", PhotonTargets.AllBuffered, 0);
                        }
                        else
                        {
                            photonView.RPC("Player4Var", PhotonTargets.AllBuffered, 1);
                        }
                    }
                    if (Player4ready == 1)
                        GUILayout.Label("yes");
                    else
                        GUILayout.Label("no");
                    GUILayout.EndHorizontal();
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();

            readycount = Player1ready + Player2ready + Player3ready + Player4ready;
            if (readycount == totalPlayers) {
				audio.Stop ();
				audio.clip = pveSong;
				audio.Play ();
                roundstart = true;
                first = true;
            }
            else {
                readycount = 0;
            }
        }
        if (roundstart && first) {
            roundstartwait = 2.0f;
            Player1ready = 0;
            Player2ready = 0;
            Player3ready = 0;
            Player4ready = 0;
            photonView.RPC("AddChatMessage", PhotonTargets.All, PhotonNetwork.player.name + " has joined the game!");
            first = false;
            SpawnMyPlayer(playerCount);
        }

        if (roundstartwait > 0)
        {
            GUILayout.Label("Round start: " + roundstartwait.ToString());
            GUILayout.Label("Spawning monsters in: " + spawnTimer.ToString());
        }

		if (pvpenabled) {
			GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			
			if (boolclick == false) {
				if (GUI.Button(new Rect(100, 100, 600, 100), thetexture))
					boolclick = true;
			}
			
			GUILayout.FlexibleSpace();
			GUILayout.EndVertical();
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
    }

    [RPC]
    void Player1Var(int num)
    {
        Player1ready = num;
    }

    [RPC]
    void Player2Var(int num)
    {
        Player2ready = num;
    }

    [RPC]
    void Player3Var(int num)
    {
        Player3ready = num;
    }

    [RPC]
    void Player4Var(int num)
    {
        Player4ready = num;
    }

	[RPC]
	void AddEnemySpawn()
	{
		numEnemiesSpawned += 1;
		if (numEnemiesSpawned >= MAX_SPAWN)
			keepSpawning = false;
	}

	[RPC]
	void EnemyDeath()
	{
		numEnemiesKilled += 1;
		if (numEnemiesKilled >= MAX_SPAWN) {
			pvpenabled = true;
		}
	}

    [RPC]
    void AddChatMessage(string s)
    {
        chatMessages.Add(s);
        msgTimeRemaining = msgDisplayTime;
        while (chatMessages.Count > maxChatMessages)
        {
            chatMessages.RemoveAt(0);
        }
    }
    
    void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed()
    {
        PhotonNetwork.CreateRoom(null);
    }

    void OnJoinedRoom()
    {
        playerCount = PhotonNetwork.playerList.Length;
        connecting = false;
        if (roundstart){
            photonView.RPC("AddChatMessage", PhotonTargets.All, PhotonNetwork.player.name + " has joined the game!");
            SpawnMyPlayer(playerCount);
        }
    }

    void SpawnMyPlayer(int count)
    {
        
        if(count == 1) {
            myPlayer = (GameObject)PhotonNetwork.Instantiate("Player1",new Vector3 (0, 2, 0), Quaternion.identity, 0);
			myPlayer.tag = "Player1";
        }
        else if (count == 2) {
            myPlayer = (GameObject)PhotonNetwork.Instantiate("Player2", new Vector3(0, 2, 0), Quaternion.identity, 0);
			myPlayer.tag = "Player2";
        }
        else if (count == 3) {
            myPlayer = (GameObject)PhotonNetwork.Instantiate("Player3", new Vector3(0, 2, 0), Quaternion.identity, 0);
			myPlayer.tag = "Player3";
        }
        else {
            myPlayer = (GameObject)PhotonNetwork.Instantiate("Player4", new Vector3(0, 2, 0), Quaternion.identity, 0);
			myPlayer.tag = "Player4";
        }

        standbyCamera.SetActive(false);

        myPlayer.GetComponent<PlayerController>().enabled = true;
        myPlayer.GetComponent<CameraSmoothFollow>().enabled = true;
        myPlayer.transform.FindChild("Camera").gameObject.SetActive(true);
        myPlayer.GetComponent<Rigidbody2D>().gravityScale = 3.5f;
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer p)
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("AddChatMessage", PhotonTargets.All, p.name + " has left.");
        }
    }

    void spawnMobs()
    {
		// Eventually this will spawn mobs at all spawn points
		GameObject[] spawns = GameObject.FindGameObjectsWithTag("Spawner");
		foreach(var point in spawns)
			if (keepSpawning)
				spawnMob(point.transform.position);
    }

    void spawnMob(Vector3 point)
    {
		photonView.RPC("AddEnemySpawn", PhotonTargets.All);
		GameObject enemy = PhotonNetwork.Instantiate("Cowboy", point, Quaternion.identity, 0);
		enemy.tag = "Enemy";
		enemy.GetComponent<EnemyController>().enabled = true;
		enemy.GetComponent<Rigidbody2D>().gravityScale = 3.5f;
    }
}
