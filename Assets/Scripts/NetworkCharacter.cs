﻿using UnityEngine;
using System.Collections;

public class NetworkCharacter : Photon.MonoBehaviour {

    Vector3 realPosition = Vector3.zero;
    Quaternion armRotation = Quaternion.identity;
    Quaternion bodyRotation = Quaternion.identity;
    Vector3 spriteScale = Vector3.zero;

    GameObject arm;
    GameObject body;
    GameObject sprite;
	GameObject hand;

	int gunActive = 0;
    bool gotFirstUpdate = false;

    Animator anim;

	// Use this for initialization
	void Start () {
        sprite = transform.FindChild("sprite").gameObject;
        body = transform.FindChild("sprite/base").gameObject;
        arm = transform.FindChild("sprite/base/arm").gameObject;
		hand = transform.FindChild("sprite/base/arm/hand").gameObject;

        anim = GetComponent<Animator>();
        if (anim == null) {
            Debug.LogError("Forgot to put animator on character prefab");
        }
        
        if (!photonView.isMine) {
            Destroy(rigidbody2D);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(!photonView.isMine) {

            transform.position = Vector3.Lerp(transform.position, realPosition, 0.3f);
            body.transform.rotation = Quaternion.Lerp(body.transform.rotation, bodyRotation, 0.35f);
            arm.transform.rotation = Quaternion.Lerp(arm.transform.rotation, armRotation, 0.35f);
            sprite.transform.localScale = spriteScale;
        }
	}

    void OnPhotonInstantiate()
    {
        if (!photonView.isMine) {
            Destroy(rigidbody2D);
        }
    }

	public void Kill()
	{
		PhotonNetwork.Destroy(gameObject);
	}

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting) { 
            //This is OUR player.  We need to send our actual position to the network.

            stream.SendNext(transform.position);
            stream.SendNext(sprite.transform.localScale);
            stream.SendNext(arm.transform.rotation);
            stream.SendNext(body.transform.rotation);

            stream.SendNext(anim.GetFloat("Speed"));
            stream.SendNext(anim.GetFloat("VertSpeed"));
            stream.SendNext(anim.GetBool("Ground"));

            
        }
        else { 
            //This is someone else's player.  We need to receive their position.
            
            realPosition = (Vector3)stream.ReceiveNext();
            spriteScale = (Vector3)stream.ReceiveNext();
            armRotation = (Quaternion)stream.ReceiveNext();
            bodyRotation = (Quaternion)stream.ReceiveNext();

            anim.SetFloat("Speed", (float)stream.ReceiveNext());
            anim.SetFloat("VertSpeed", (float)stream.ReceiveNext());
            anim.SetBool("Ground", (bool)stream.ReceiveNext());

            if (gotFirstUpdate == false) {
                transform.position = realPosition;
                gotFirstUpdate = true;
            }
        }
    }
}
