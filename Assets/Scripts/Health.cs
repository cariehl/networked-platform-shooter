﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{

    public float hitpoints = 100f;
    float currentHitPoints;

    // Use this for initialization
    void Start()
    {
        currentHitPoints = hitpoints;
    }

    public void TakeDamage(float amt)
    {
        currentHitPoints -= amt;

        if (currentHitPoints <= 0)
        {
            Die();
        }
    }

    void Die()
    {
		if (this.tag == "Enemy")
			GetComponent<NetworkEnemy>().Kill();
		else if (this.tag.StartsWith("Player"))
			GetComponent<NetworkCharacter>().Kill();
		else
			Destroy(gameObject);
    }
}